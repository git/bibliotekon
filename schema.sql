/*
 * This file is part of Bibliotekon.
 *       
 * Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
 *
 * Bibliotekon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bibliotekon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

create table if not exists core_user (
	id int(6) auto_increment,
        username varchar(20),
	email varchar(100),
	passwd varchar(500) not null,
	name varchar(50),
	lname varchar(100),
	constraint pk_user primary key (id),
	constraint un_user1 unique (email),
        constraint un_user2 unique (username)
) engine=innodb, charset=utf8;


create table if not exists publisher (
	id int(7) auto_increment,
	name varchar(50) not null,
	constraint pk_publisher primary key (id),
	constraint un_publisher unique (name)
) engine=innodb, charset=utf8;


create table if not exists format (
	id int(7) auto_increment,
	name varchar(50) not null,
	constraint pk_format primary key (id),
	constraint un_format unique (name)
) engine=innodb, charset=utf8;


create table if not exists book (
       id int(6) auto_increment,
       isbn varchar(30) not null,
       title varchar(100) not null,
       description longtext,
       cover varchar(200),
       publisher int(7),
       format int(7),
       pages int(4),
       constraint pk_book primary key (id),
       constraint un_book unique (isbn),
       constraint fk_book_publisher foreign key (publisher) references publisher(id) on delete set null on update cascade,
       constraint fk_book_format foreign key (format) references format(id) on delete set null on update cascade
) engine=innodb, charset=utf8;


create table if not exists author (
       id int(6) auto_increment,
       name varchar(30) not null,
       surname varchar(50) not null,
       photo varchar(200),
       constraint pk_author primary key (id),
       constraint un_author unique (name, surname)
) engine=innodb, charset=utf8;


create table if not exists book_author (
       id int(7) auto_increment,
       book int(6) not null,	-- primary key of utam_book table.
       author int(6) not null,	-- primary key of utam_author table.
       constraint pk_book_author primary key (id),
       constraint un_book_author unique (book, author),
       constraint fk_book_author_book foreign key (book) references book(id) on delete cascade on update cascade,
       constraint fk_book_author_author foreign key (author) references author(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists subject (
       id int(2) auto_increment,
       name varchar(100) not null,
       constraint pk_subject primary key (id),
       constraint un_subject unique (name)
) engine=innodb, charset=utf8;


create table if not exists book_subject (
       id int(7) auto_increment,
       book int(6) not null,	-- primary key of utam_book table.
       subject int(6) not null,	-- primary key of utam_subject table.
       constraint pk_book_subject primary key (id),
       constraint un_book_subject unique (book, subject),
       constraint fk_book_subject_book foreign key (book) references book(id) on delete cascade on update cascade,
       constraint fk_book_subject_subject foreign key (subject) references subject(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists user_book (
       id int(6) auto_increment,
       book int(6) not null,
       core_user int(6) not null,
       private int(1) default 1, -- The book information is public but your opinion, etc maybe not.
       constraint pk_user_book primary key (id),
       constraint un_user_book unique (book, core_user),
       constraint fk_user_book_book foreign key (book) references book(id) on delete cascade on update cascade,
       constraint fk_user_book_core_user foreign key (core_user) references core_user(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists book_readed (
       id int(6) auto_increment,
       start date,			-- started read date.
       finish date,			-- finished read date.
       opinion longtext,		-- my personal opinion about this read book.
       valoration int(2),		-- my personal valoration (1 - 10).
       constraint pk_read primary key (id),
       constraint fk_read_book foreign key (id) references user_book(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists book_wishlist (
       id int(6) auto_increment,
       constraint pk_wishlist primary key (id),
       constraint fk_wishlist_book foreign key (id) references user_book(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


/******************************* DIRECTIONS ***********************************/
create table if not exists continent (
       id int(1) auto_increment,
       name varchar(50) not null,
       constraint pk_continent primary key (id)
) engine=innodb, charset=utf8;
insert into continent (name) values ('Asia');
insert into continent (name) values ('Europe');
insert into continent (name) values ('North America');
insert into continent (name) values ('South America');
insert into continent (name) values ('Africa');
insert into continent (name) values ('Oceania');
insert into continent (name) values ('Antarctica');


create table if not exists country (
       id int(6) auto_increment,
       countryid varchar(3),
       name varchar(50) not null,
       continent int(1) not null,
       latitude double,
       longitude double,
       constraint pk_country primary key (id),
       constraint fk_country_continent foreign key (continent) references continent(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists region (
       id int(6),
       name varchar(50) not null,
       country int(6) not null,
       latitude double,
       longitude double,
       constraint pk_region primary key (id),
       constraint fk_region_country foreign key (country) references country(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists city (
       id int(6),
       name varchar(50) not null,
       region int(6) not null,
       latitude double,
       longitude double,
       constraint pk_city primary key (id),
       constraint fk_city_state foreign key (region) references region(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists street (
       id int(7) auto_increment,
       name varchar(100) not null,
       num varchar(4),
       code varchar(10),
       extra varchar(100),
       city int(6) not null,
       latitude double,
       longitude double,
       constraint pk_street primary key (id),
       constraint fk_street_city foreign key (city) references city(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;
/****************************** END DIRECTIONS ********************************/

create table if not exists bookshop_address (
       id int(7) auto_increment,
       street int(7) not null,
       constraint pk_address primary key (id),
       constraint fk_address_street foreign key (street) references street(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists bookshop (
	id int(6) auto_increment,
	name varchar(100),
	logo varchar(200),
	constraint pk_bookshop primary key (id)
) engine=innodb, charset=utf8;

-- Hay dos tipos de bookshop: aquellos que tiene dirección física y los que son online.
-- Un bookshop, puede ser de los dos tipos.
create table if not exists addr_bookshop (
       id int(6) auto_increment,
       address int(7),
       constraint pk_addr_bookshop primary key (id),
       constraint fk_bookshop_bookshop foreign key (id) references bookshop(id) on delete cascade on update cascade,
       constraint fk_bookshop_address foreign key (address) references bookshop_address(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists online_bookshop (
       id int(6) auto_increment,
       url varchar(200) not null,
       constraint pk_online_bookshop primary key (id),
       constraint un_online_bookshop unique (url),
       constraint fk_online_bookshop foreign key (id) references bookshop(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists purchased (
       id int(6) auto_increment,
       price float not null, -- price in Euros.
       bookshop int(6),
       purchased date,
       constraint pk_purchased primary key (id),
       constraint fk_purchased_read foreign key (id) references book_readed(id) on delete cascade on update cascade,
       constraint fk_purchased_bookshop foreign key (bookshop) references bookshop(id) on delete set null on update cascade
) engine=innodb, charset=utf8;



















create table if not exists ajen_contact (
       id int(6) auto_increment,
       name varchar(30) not null,
       surname varchar(50),
       nick varchar(30),
       address varchar(300),
       birthday date,
       constraint pk_contact primary key (id)
) engine=innodb, charset=utf8;


create table if not exists utam_loaned (
       id int(6) auto_increment,
       isbn varchar (30) not null,
       contact int(6) not null,		-- Contact identifier.
       constraint pk_loaned primary key (id),
       constraint un_loaned unique (isbn),
       constraint fk_loaned_read foreign key (id) references utam_read(id) on delete cascade on update cascade,
       constraint fk_loaned_contact foreign key (contact) references ajen_contact(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists utam_quote (
       id int(6) auto_increment,
       quote varchar(1000) not null,
       author varchar(200),
       constraint pk_quote primary key (id)
) engine=innodb, charset=utf8;


create table if not exists utam_book_quote (
       id int(6) auto_increment,
       page int(3),
       book int(6) not null,	-- Book readed identifier where is the quote.
       constraint pk_book_quote primary key (id),
       constraint fk_book_quote_quote foreign key (id) references utam_quote(id) on delete cascade on update cascade,
       constraint fk_book_quote_book foreign key (book) references utam_read(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists ajen_tag (
       id int(6) auto_increment,
       name varchar(50) not null,
       constraint pk_tag primary key (id),
       constraint un_tag unique (name)
) engine=innodb, charset=utf8;


create table if not exists ajen_contact_tag (
       id int(6) auto_increment,
       contact int(6), -- Contact primary key references.
       tag int(6),     -- Tag primary key references.
       constraint pk_contact_tag primary key (id),
       constraint un_contact_tag unique (contact, tag),
       constraint fk_contact_tag_contact foreign key (contact) references ajen_contact(id) on delete cascade on update cascade,
       constraint fk_contact_tag_tag foreign key (tag) references ajen_tag(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists ajen_email (
       id int(7) auto_increment,
       email varchar(200) not null,
       contact int(6) not null,		-- Contact primary key references.
       constraint pk_email primary key (id),
       constraint fk_email_contact foreign key (contact) references ajen_contact(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;


create table if not exists ajen_phone (
       id int(7) auto_increment,
       numphone varchar(50) not null,
       contact int(6) not null,		-- Contact primary key references.
       constraint pk_phone primary key (id),
       constraint fk_phone_contact foreign key (contact) references ajen_contact(id) on delete cascade on update cascade
) engine=innodb, charset=utf8;
