# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import os

_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
SECRET_KEY = 'asdfñj asdfñj saljf lasjf lñasjf lasjfls'

# The languages for i18n and l10n with Flask-Babel.
LANGUAGES = {
    'en': 'English',
    'es': 'Español'
    }

# The application folder (absolute path). (OLD - relative path from run.py file).
APP_FOLDER = _basedir + '/bibliotekon'

# If the app is installed inside of directory and it does not the root.
PREFIX = ''

STATIC_DIR = 'static'
UPLOAD_FOLDER = 'static/uploads'
COVER_SUBFOLDER = 'cover'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

DATABASE = 'mysql://roman:roman@localhost/bibliotekon?charset=utf8'
USERNAME = 'roman'
PASSWORD = 'roman'
