# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, Integer, String, Text, ForeignKey, Date, Float
from sqlalchemy import UniqueConstraint, Table
from sqlalchemy.orm import relationship, backref
from bibliotekon.auth.models import CoreUser
from bibliotekon.db.database import Base


class Publisher(Base):
    __tablename__ = 'publisher'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)

    books = relationship('Book', backref='br_publisher', lazy='dynamic')


class Format(Base):
    __tablename__ = 'format'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)

    books = relationship('Book', backref='br_format', lazy='dynamic')


book_author = Table('book_author', Base.metadata,
                    Column('book', Integer, ForeignKey('book.id')),
                    Column('author', Integer, ForeignKey('author.id')))

book_subject = Table('book_subject', Base.metadata,
                     Column('book', Integer, ForeignKey('book.id')),
                     Column('subject', Integer, ForeignKey('subject.id')))


class UserBook(Base):
    __tablename__ = 'user_book'
    __table_args = {'mysql_engine':'InnoDB'}

    id = Column(Integer, primary_key=True)
    book = Column(ForeignKey('book.id'), nullable=False)
    core_user = Column(ForeignKey('core_user.id'), nullable=False, index=True)
    private = Column(Integer, server_default="'1'")

    book_obj = relationship('Book')
    core_user_obj = relationship('CoreUser')


class Book(Base):
    __tablename__ = 'book'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    isbn = Column(String(30), nullable=False, unique=True)
    title = Column(String(100), nullable=False)
    description = Column(Text)
    cover = Column(String(200))
    pages = Column(Integer)

    publisher = Column(Integer, ForeignKey('publisher.id'))
    format = Column(Integer, ForeignKey('format.id'))

    authors = relationship('Author', secondary=book_author, backref="br_books")
    subjects = relationship('Subject', secondary=book_subject, backref="br_books")


    users = relationship('UserBook',
                         backref=backref('br_book'), 
                         lazy='dynamic',
                         cascade='merge, save-update, delete')


class Author(Base):
    __tablename__ = 'author'
    __table_args__ = (UniqueConstraint('name', 'surname'),
                      {'mysql_engine':'InnoDB'})
    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(50), nullable=False)
    photo = Column(String(200))


class Subject(Base):
    __tablename__ = 'subject'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=True)


class BookReaded(Base):
    __tablename__ = 'book_readed'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, ForeignKey('user_book.id'), primary_key=True)
    start = Column(Date)
    finish = Column(Date)
    opinion = Column(Text)
    valoration = Column(Integer)

    purchased = relationship('Purchased',
                             backref=backref('br_bookreaded'), 
                             lazy='dynamic',
                             cascade='merge, save-update, delete')


class BookWishlist(Base):
    __tablename__ = 'book_wishlist'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, ForeignKey('user_book.id'), primary_key=True)


class Continent(Base):
    __tablename__ = 'continent'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)

    countries = relationship('Country', backref='br_continent', lazy='dynamic')


class Country(Base):
    __tablename__ = 'country'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    countryid = Column(String(3))
    name = Column(String(50), nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)

    regions = relationship('Region', backref='br_country', lazy='dynamic')

    continent = Column(Integer, ForeignKey('continent.id'))


class Region(Base):
    __tablename__ = 'region'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)

    cities = relationship('City', backref='br_region', lazy='dynamic')

    country = Column(Integer, ForeignKey('country.id'))


class City(Base):
    __tablename__ = 'city'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)

    streets = relationship('Street', backref='br_city', lazy='dynamic')

    region = Column(Integer, ForeignKey('region.id'))


class Street(Base):
    __tablename__ = 'street'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    num = Column(Integer)
    code = Column(String(10))
    extra = Column(String(100))
    latitude = Column(Float)
    longitude = Column(Float)

    city = Column(Integer, ForeignKey('city.id'))


class BookshopAddress(Base):
    __tablename__ = 'bookshop_address'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)

    addrbookshop = relationship('AddrBookshop', backref='br_bookshopaddress',
                                lazy='dynamic')


class Bookshop(Base):
    __tablename__ = 'bookshop'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    logo = Column(String(200))

    addrbookshop = relationship('AddrBookshop', backref='br_addrbookshop',
                                lazy='dynamic')
    onlinebookshop = relationship('OnlineBookshop', backref='br_onlinebookshop',
                                  lazy='dynamic')
    purchaseds = relationship('Purchased', backref='br_purchasedbookshop',
                              lazy='dynamic')


class AddrBookshop(Base):
    __tablename__ = 'addr_bookshop'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, ForeignKey('bookshop.id'), primary_key=True)

    address = Column(Integer, ForeignKey('bookshop_address.id'))


class OnlineBookshop(Base):
    __tablename__ = 'online_bookshop'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, ForeignKey('bookshop.id'), primary_key=True)
    url = Column(String(200), nullable=False, unique=True)


class Purchased(Base):
    __tablename__ = 'purchased'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, ForeignKey('book_readed.id'), primary_key=True)
    price = Column(Float, nullable=False)
    purchased = Column(Date)

    bookshop = Column(Integer, ForeignKey('bookshop.id'))
