# -*- coding: utf-8 -*-

# This file is part of PyOL (Python Open Library).
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# PyOL is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyOL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

import urllib2
import json


class Publisher:
    """ Publisher class.
    This class represent an OpenLibrary.org publisher.
    """
    def __init__(self, name = None):
        self.name = name


    @staticmethod
    def get_publisher_from_dict(p):
        return Publisher(p['name'] if 'name' in p else None)


class Identifier:
    """ Identifier class.

    This class represent an OpenLibrary.org identifier.

    The bibkey (identifiers) can be: google, lccn, isbn_13, amazon, isbn_10,
    oclc, librarything, project_gutenberg or goodreads identifier.

    The id is a list of values associated to the bibkey (identifier).

    Example: 
    "isbn_13": [
                "9780980200447",
                "0980200447"
            ]

    The "isbn_13" is the bibkey and [ ... ] the list of ids.
    """
    def __init__(self, id = None, bibkey = None):
        self.id = id
        self.bibkey = bibkey


    @staticmethod
    def get_identifier_from_pair(key, val):
        return Identifier(val, key)


class Classification:
    """ Classification class.

    This class represent an OpenLibrary.org classification.

    Example: 
    "lc_classifications": [
                "Z1003 .M58 2009"
            ]
    """
    def __init__(self, val = None, key = None):
        self.val = val
        self.key = key


    @staticmethod
    def get_classification_from_pair(key, val):
        return Classification(val, key)


class Link:
    """ Link class.

    This class represent an OpenLibrary.org link. The link is represented by a
    dictionary with 'url' and 'title' keys.

    Example from OpenLibrary of links section:
    [
       {
          "url": "http://johnmiedema.ca",
          "title": "Author's Website"
       }
    ]
    """
    def __init__(self, url = None, title = None):
        self.url = url
        self.title = title


    @staticmethod
    def get_link_from_dict(url, title):
        return Link(url, title)


class Subject:
    """ Subject class.

    This class represent an OpenLibrary.org subject. The link is represented by a
    dictionary with 'url' and 'name' keys.

    Example from OpenLibrary of subjects section:
    [
       {
          "url": "http://openlibrary.org/subjects/books_and_reading",
          "name": "Books and reading"
       },
       {
          "url": "http://openlibrary.org/subjects/reading",
          "name": "Reading"
       }
    ]
    """
    def __init__(self, url = None, name = None):
        self.url = url
        self.name = name


    @staticmethod
    def get_subject_from_dict(url, name):
        return Subject(url, name)


class Author:
    """ Author class.

    This class represent an OpenLibrary.org author. The link is represented by a
    dictionary with 'url' and 'name' keys.

    Example from OpenLibrary of authors section:
    [
       {
          "url": "http://openlibrary.org/authors/OL6548935A/John_Miedema",
          "name": "John Miedema"
       }
    ]
    """
    def __init__(self, url = None, name = None):
        self.url = url
        list_aux = name.split()
        self.name = list_aux[0] if list_aux else ''
        self.surname = ' '.join(list_aux[1:]) if list_aux else ''


    @staticmethod
    def get_author_from_dict(url, name):
        return Author(url, name)


class Excerpt:
    """ Excerpt class.

    This class represent an OpenLibrary.org excerpt. The link is represented by a
    dictionary with 'comment' and 'text' keys.

    Example from OpenLibrary of excerpts section:
    [
       {
          "comment": "test purposes",
          "text": "test first page"
       }
    ]
    """
    def __init__(self, comment = None, text = None):
        self.comment = comment
        self.text = text


    @staticmethod
    def get_excerpt_from_dict(comment, text):
        return Excerpt(comment, text)


class PublishPlace:
    """ PublishPlace class.
    This class represent an OpenLibrary.org publish_place.
    """
    def __init__(self, name = None):
        self.name = name


    @staticmethod
    def get_publish_place_from_dict(p):
        return PublishPlace(p['name'] if 'name' in p else None)


class Book:
    """ Book class.
    This class represent an OpenLibrary.org book.
    """
    def __init__(self):
        """ Initialize the Book object with all information from OpenLibrary.org.
        You can view a complete example here:
        http://openlibrary.org/dev/docs/api/books
        """
        self.publishers = None
        self.identifiers = None
        self.classifications = None
        self.links = None
        self.weight = None
        self.title = None
        self.subtitle = None
        self.url = None
        self.number_of_pages = None
        self.cover = None
        self.subjects = None
        self.publish_date = None
        self.authors = None
        self.excerpts = None
        self.publish_places = None


    def create_book_from_json(self, json):
        """ Create a book from json object.
        """
        self.publishers = [ Publisher.get_publisher_from_dict(p) \
                                for p in json['publishers'] ] \
                                if 'publishers' in json else None
        self.identifiers = [ 
            Identifier.get_identifier_from_pair(key, val) \
                for key, val in json['identifiers'].iteritems()
            ] \
            if 'identifiers' in json else None
        self.classifications = [ 
            Classification.get_classification_from_pair(key, val) \
                for key, val in json['classifications'].iteritems()
            ] \
            if 'classifications' in json else None
        self.links = [ Link.get_link_from_dict(d['url'], d['title']) \
                           for d in json['links'] ] \
                           if 'links' in json else None
        self.weight = json['weight'] if 'weight' in json else None
        self.title = json['title'] if 'title' in json else None
        self.subtitle = json['subtitle'] if 'subtitle' in json else None
        self.url = json['url'] if 'url' in json else None
        self.number_of_pages = json['number_of_pages'] if 'number_of_pages' in json else None
        self.cover = json['cover']['large'] if 'cover' in json else None
        self.subjects = [ Subject.get_subject_from_dict(d['url'], d['name']) \
                              for d in json['subjects'] ]  \
                              if 'subjects' in json else None
        self.publish_date = json['publish_date'] if 'publish_date' in json else None
        self.authors = [ Author.get_author_from_dict(d['url'], d['name']) \
                             for d in json['authors'] ] \
                             if 'authors' in json else None
        self.excerpts = [ Excerpt.get_excerpt_from_dict(d['comment'], d['text']) \
                              for d in json['excerpts'] ] \
                              if 'excerpts' in json else None
        self.publish_places = self.publish_places = [ 
            PublishPlace.get_publish_place_from_dict(p) \
                for p in json['publish_places'] 
            ] \
            if 'publish_places' in json else None
        


class OpenLibrary:
    """ The API to use the OpenLibrary wrap.
    
    It is very easy to use. You only have to do these two steps:
    1) Create the OpenLibrary object:
       ol = OpenLibrary()
    
    2) Get the book from OpenLibrary object indicating the ISBN or other bibkey:
       book = ol.get_book('9780980200447')

    Then, see the Book class above to see all book attributes.
    """
    def __init__(self, base_url = 'http://openlibrary.org/api/books?bibkeys='):
        self.base_url = base_url


    def get_book(self, id, bibkey="ISBN"):
        """ This function return a book which his "bibkey" is equal to "id".
        """
        url = self.base_url + bibkey + ':' + id + '&jscmd=data&format=json';
        page_source = urllib2.urlopen(url)
        ol_json = json.load(page_source)['%s:%s' % (bibkey, id)]
        
        book = Book()
        book.create_book_from_json(ol_json)

        return book
