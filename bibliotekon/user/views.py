# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from flask import Blueprint, session, render_template, request, flash
from flask.ext.babel import gettext

from sqlalchemy.exc import SQLAlchemyError

from bibliotekon import app
from bibliotekon.auth.models import CoreUser
from bibliotekon.auth.lib import login_required
from bibliotekon.auth.forms import RegistrationForm
from bibliotekon.auth.tools import update_user
from bibliotekon.db.database import db_session


mod = Blueprint('user', __name__, template_folder = 'templates')


@mod.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    """The user home.
    
    Show the user information.
    """
    user = CoreUser.query.get(session['user_id'])
    return render_template('home.html', user=user)


@mod.route('/update', methods=['GET', 'POST'])
def update():
    """The user update information view.

    Update the user information.
    """
    form = RegistrationForm()
    user = CoreUser.query.get(session['user_id'])
    if request.method == 'POST' and form.validate():
        try:
            user.name = form.name.data if form.name.data else ''
            user.surname = form.lname.data if form.lname.data else ''
            user.email = form.email.data if form.email.data else ''
            user.username = form.username.data if form.username.data else ''
            if form.passwd.data or form.passwdrepeat.data:
                user.passwd = form.passwd.data                
            update_user(user, form)
            flash(gettext('The profile have been updated.'), 'success')
        except SQLAlchemyError, e:
            flash(gettext('There have been a database error.'), 'error')
            app.logger.error('There have been a database error: %s' % e)
            db_session.rollback()
        except Exception, e:
            flash(gettext('There have been a weird error.'), 'error')
            app.logger.error('There have been a weird error: %s' % e)
            db_session.rollback()

    return render_template('update.html', user=user, form=form)
