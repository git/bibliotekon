# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from flask.ext.wtf import Form, TextField, TextAreaField, DateField, Required
from flask.ext.wtf import IntegerField, DecimalField, length, Optional
from flask.ext.wtf import FileField, HiddenField, ValidationError
from wtforms.validators import StopValidation
from flask.ext.babel import gettext

from util import isbn


def IsbnValidator(form, field):
    """
    Check if the field is a valid ISBN
    """
    try:
        if not isbn.isValid(form.isbn.data):
            raise ValidationError(gettext('The ISBN is not valid.'))
    except Exception, e:
        raise ValidationError(gettext('The ISBN is not valid.'))


def PurchasedValidator(form, field):
    """
    Check if there is price when the form come with purchased information (with
    bookshop and/or date purchased information).
    """
    field.errors[:] = []
    if (form.bookshop.data or form.purchased.data) and not form.price.data:
        raise StopValidation(gettext('If you enter purchased information then the price (a decimal number) is required'))
    else:
        raise StopValidation()


class BookForm(Form):
    # The basic information
    cover = FileField('Cover', [Optional()])
    title = TextField('Title', [Required('The title is required.'),
                                length(max=100)])
    isbn = TextField('ISBN', [Required('The ISBN is required.'),
                              IsbnValidator,
                              length(max=30)])
    format = TextField('Format', [length(max=50), Optional()])
    subject = TextField('Subject', [length(max=100), Optional()])
    pages = IntegerField('Pages', [Optional()])
    author = TextField('Author', [length(max=80), Optional()])
    publisher = TextField('Publisher', [length(max=50), Optional()])
    description = TextAreaField('Description')

    # Cover field hidden
    hidden_cover = HiddenField()

class BookReadedForm(Form):
    # Book readed information
    start = DateField('Start', [Optional()])
    finish = DateField('Finish', [Optional()])
    opinion = TextAreaField('Opinion')
    valoration = IntegerField('Valoration', [Optional()])

    # Book purchased
    price = DecimalField('Price', [PurchasedValidator])
    purchased = DateField('Purchased date', [Optional()])
    bookshop = TextField('Bookshop', [length(max=100)])
