# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from flask import flash, session

from sqlalchemy import func, and_

from bibliotekon import app
from bibliotekon.forms import BookForm
from bibliotekon.models import Book, Format, Publisher, Subject, Author
from bibliotekon.models import Bookshop, BookReaded, Purchased, CoreUser
from bibliotekon.models import UserBook, BookWishlist
from bibliotekon.db.database import db_session


def bookform2book(form, book):
    """Convert a BookForm object to Book model.
    
    It converts from BookForm object to Book model including all its database
    models (BookReaded, Format, Purshased, etc - you can look at models.py file.
    """
    try:
        # Basic book attributes.
        book.isbn = None if not form.isbn.data else form.isbn.data
        book.title = None if not form.title.data else form.title.data
        book.description = None if not form.description.data else form.description.data
        book.cover = None if not form.cover.data else form.cover.data
        book.pages = None if not form.pages.data else form.pages.data

        # The Format
        if form.format.data:
            search = (Format.name == form.format.data)
            format_obj = Format.query.filter(search).first()
            if format_obj: # If the format already exist, I only need the id.
                book.format = format_obj.id
            else:          # Otherwise, I create the new format that will insert.
                book.br_format = Format()
                book.br_format.name = form.format.data

        # The Publisher
        if form.publisher.data:
            search = (Publisher.name == form.publisher.data)
            publisher_obj = Publisher.query.filter(search).first()
            if publisher_obj: # If the publisher already exist, I only need the id.
                book.publisher = publisher_obj.id
            else:             # Otherwise, I create the new publisher that will insert.
                book.br_publisher = Publisher()
                book.br_publisher.name = form.publisher.data

        # The Subjects
        if form.subject.data:
            book.subjects = []
            subject_list = form.subject.data.split(',')
            for subject_str in subject_list: # for each subject inserted...
                subject_str = subject_str.strip()
                subject = Subject()
                subject.name = subject_str
                # We need to check if the subject already exist.
                search = (Subject.name == subject_str)
                subject_obj = Subject.query.filter(search).first()
                if subject_obj:
                    book.subjects.append(subject_obj)
                else:
                    book.subjects.append(subject)

        # The Authors
        if form.author.data:
            book.authors = []
            author_list = form.author.data.split(';')
            for author_str in author_list:
                lname_name = author_str.split(',')
                if len(lname_name) != 2:
                    flash('The author name format is not valid.', 'error')
                    return False

                author = Author()
                author.name = lname_name[1].strip()
                author.surname = lname_name[0].strip()
                # We need to check if the author already exist.
                search = (func.lower(Author.name) == 
                          func.lower(author.name.lower))
                search = and_(func.lower(Author.surname) == 
                              func.lower(author.surname))
                author_obj = Author.query.filter(search).first()
                if author_obj:
                    book.authors.append(author_obj)
                else:
                    book.authors.append(author)
    except AttributeError, e:
        flash('There are application errors. You can contact with sysadmin of the site.',
              'error')
        app.logger.error('There have been an application error: %s' % e)
        raise Exception(e)

    return True


def bookreadedform2bookreaded(form, book_id, br):
    """Convert a BookReadedForm object to BookReaded model.
    
    It converts from BookReadedForm object to Book model including all its database
    models (Purshased, etc - you can look at models.py file). For that task we
    need the book identifier.
    """
    try:
        # The book readed
        if form.start.data or form.finish.data or form.opinion.data or form.valoration.data or form.price.data or form.purchased.data:
            purchased = None
            is_there_br = False
            is_there_purchased = False
            ub = None

            if 'user_id' in session:
                br_aux = BookReaded \
                    .query \
                    .join(UserBook, UserBook.id == BookReaded.id) \
                    .filter(UserBook.book == book_id) \
                    .filter(UserBook.core_user == session['user_id']) \
                    .first()
            else:
                return False

            if not br_aux:
                ub = UserBook \
                    .query \
                    .filter(UserBook.core_user == session['user_id']) \
                    .filter(UserBook.book == book_id).first()
                if not ub:
                    return False
                br.id = ub.id
            else:
                br.id = br_aux.id
                is_there_br = True

            br.start = None if not form.start.data else form.start.data
            br.finish = None if not form.finish.data else form.finish.data
            br.opinion = None if not form.opinion.data else form.opinion.data
            br.valoration = None if not form.valoration.data else form.valoration.data
            # If is the book purchased
            if form.price.data or form.purchased.data:
                for purchased in br.purchased:
                    is_there_purchased = True
                if not purchased:
                    purchased = Purchased()
                purchased.price = None if not form.price.data else form.price.data
                purchased.purchased = None if not form.purchased.data \
                    else form.purchased.data
                # If there is bookshop
                if form.bookshop.data:
                    search = (Bookshop.name == form.bookshop.data)
                    bookshop_obj = Bookshop.query.filter(search).first()
                    if bookshop_obj: # If the bookshop already exist, I only need the id.
                        purchased.bookshop = bookshop_obj.id
                    else:          # Otherwise, I create the new bookshop that will insert.
                        purchased.br_purchasedbookshop = Bookshop()
                        purchased.br_purchasedbookshop.name = form.bookshop.data
                # If there were a purchased we have not to append it (it will be update).
                if not is_there_purchased:
                    br.purchased.append(purchased)
    except AttributeError, e:
        flash('There are application errors. You can contact with sysadmin of the site.',
              'error')
        app.logger.error('There have been an application error: %s' % e)
        raise Exception(e)

    return True


def get_wishlist_from_book_id(book_id):
    """Return the wishlist book if the book with 'book_id' is a 
    wishlist book. Otherwise return None.
    """
    the_query = BookWishlist.query \
        .join(UserBook, UserBook.id == BookWishlist.id) \
        .filter(UserBook.book == book_id)

    if the_query.count():
        print "bieeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeen"
        return the_query.first()
    else:
        return None
