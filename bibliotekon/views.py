# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

import os, shutil, json, errno

from flask import request, render_template, flash, send_from_directory, redirect
from flask import url_for, g, session
from flask.ext.babel import gettext

from werkzeug.utils import secure_filename

from sqlalchemy import and_, func, not_
from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from bibliotekon import app
from bibliotekon.forms import BookForm, BookReadedForm
from bibliotekon.models import Book, Format, Publisher, Subject, Author
from bibliotekon.models import Bookshop, BookReaded, Purchased, UserBook
from bibliotekon.models import BookWishlist
from bibliotekon.auth.lib import login_required
from bibliotekon.db.database import db_session, init_db
from bibliotekon.util.file import make_sure_path_exists, allowed_file
from bibliotekon.common import bookform2book, bookreadedform2bookreaded
from bibliotekon.common import get_wishlist_from_book_id
from bibliotekon.auth.models import CoreUser

from bibliotekon.lib.pyol import OpenLibrary


@app.route('/bookbyisbn/<isbn>')
@login_required
def book_by_isbn(isbn):
    """Return the basic book information through JSON format.

    This function can be used through AJAX (jQuery getJSON function) to get book
    basic information in JSON format.
    """
    json_data = {}
    search = (Book.isbn == isbn)
    book = Book.query.filter(search).first()
    # Firstly see into our database.
    if book:
        json_data['id'] = book.id
        json_data['title'] = book.title
        json_data['description'] = book.description
        json_data['cover'] = book.cover
        json_data['pages'] = book.pages
        json_data['format'] = book.br_format.name if book.br_format else ''
        json_data['subject'] = ', '.join([ b.name for b in book.subjects ]) \
            if book.subjects else ''
        json_data['author'] = '; '.join([ a.surname + ', ' + a.name \
                                              for a in book.authors ])
        json_data['publisher'] = book.br_publisher.name \
            if book.br_publisher else ''
        if session and 'user_id' in session:
            json_data['core_user'] = session['user_id'] in [user.id for user in book.users ]
        else:
            json_data['core_user'] = False
    # If is not the book into our database then we load information from OpenLibrary
    else:
        ol = OpenLibrary()
        book = ol.get_book(isbn)
        json_data['title'] = book.title + (', ' + book.subtitle if book.subtitle else '') \
            if book.title else ''
        json_data['cover'] = book.cover if book.cover else ''
        json_data['description'] = book.excerpts[0].comment if book.excerpts else ''
        json_data['pages'] = book.number_of_pages
        json_data['subject'] = ', '.join([ b.name for b in book.subjects ]) \
            if book.subjects else ''
        json_data['author'] = '; '.join([ a.surname + ', ' + a.name \
                                              for a in book.authors ]) \
                                              if book.authors else ''
        json_data['publisher'] = ', '.join([p.name for p in book.publishers ]) \
            if book.publishers else ''


    return json.dumps(json_data)


@app.route('/book/<where>/<int:book_id>')
def book(where, book_id):
    """The book view (details).

    Get the id of the book and show it.
    """
    book = Book.query.get(book_id)

    # Only the owner can view the book readed, purchased and wishlist
    # information.
    bookreaded = None
    if 'user_id' in session and book:
        bookreaded = BookReaded \
            .query \
            .join(UserBook, UserBook.id == BookReaded.id) \
            .join(Book, Book.id == UserBook.book) \
            .filter(Book.id == book.id) \
            .filter(UserBook.core_user == session['user_id']).first()

    return render_template('book.html', book=book, bookreaded=bookreaded, where=where)


@app.route('/delbook/<int:book_id>', methods=['GET', 'POST'])
@login_required
def delbook(book_id):
    """the delete book view.

    Get the id of the book and delete it.
    """
    try:
        db_session.begin()
        book = Book.query.get(book_id)
        if book.cover:
            shutil.rmtree(os.path.join(app.config['APP_FOLDER'],
                                       app.config['UPLOAD_FOLDER'],
                                       app.config['COVER_SUBFOLDER'],
                                       book.isbn))
        db_session.delete(book)
        db_session.commit()
        flash(gettext('The book has been deleted'), 'success')
    except SQLAlchemyError, e:
        flash(gettext('There have been a database error.'), 'error')
        app.logger.error('There have been a database error: %s' % e)
        db_session.rollback()
    except IOError, e:
        flash(gettext('There have been an error deleting the cover image.'), 'error')
        app.logger.error('There have been an error deleting the cover image: %s' % e)
        db_session.rollback()
    except Exception, e:
        flash(gettext('There have been a weird error.'), 'error')
        app.logger.error('There have been a weird error: %s' % e)
        db_session.rollback()
    return redirect(url_for('index'))


@app.route('/changebook/<int:book_id>', methods=['GET', 'POST'])
@login_required
def changebook(book_id):
    """Change book view.

    Get the id of the book to change and show the form to update it.
    """
    ok = False
    g.idbook = book_id
    img = None
    readed = False
    form = BookForm(request.form)
    book = Book.query.get(book_id)
    
    # If the book existed but the user had not this book...
    if 'user_id' in session:
        userbook = UserBook \
            .query \
            .filter(UserBook.book == book_id) \
            .filter(UserBook.core_user == session['user_id']) \
            .first()
        if not userbook:
            userbook = UserBook()
            userbook.core_user = CoreUser.query.get(session['user_id']).id
            book.users.append(userbook)
        else:
            if BookReaded.query.get(userbook.id):
                readed = True

    if request.method == 'POST' and form.validate_on_submit():
        try:
            db_session.begin()
            old_isbn = book.isbn # We save the current ISBN before get new data
                                 # from the form.

            bookform2book(form, book) # Get and set the book with the nuew data
                                      # from the form.

            # The cover (is a file)
            file = request.files['cover'] if request.files else None
            # If user uploaded a file... then we have to delete the older file
            # and save the new file.
            if file:
                filename = secure_filename(file.filename)
                old_cover_path = os.path.join(app.config['APP_FOLDER'],
                                          app.config['UPLOAD_FOLDER'], 
                                          app.config['COVER_SUBFOLDER'],
                                          old_isbn)
                cover_path = os.path.join(app.config['APP_FOLDER'],
                                          app.config['UPLOAD_FOLDER'], 
                                          app.config['COVER_SUBFOLDER'],
                                          book.isbn)
                if os.path.exists(old_cover_path):
                    shutil.rmtree(old_cover_path)
                cover_filename_path = os.path.join(cover_path, filename)
                make_sure_path_exists(cover_path)
                file.save(cover_filename_path)
                img = os.path.join('/',
                                   app.config['UPLOAD_FOLDER'], 
                                   app.config['COVER_SUBFOLDER'],
                                   book.isbn,
                                   filename)
                book.cover = img
            # If the user does not upload a new image then the current image
            # information is in the form hidden field.
            elif form.hidden_cover.data:
                if old_isbn != book.isbn:
                    filename = form.hidden_cover.data.split('/')[-1]
                    old_cover_path = os.path.join(app.config['APP_FOLDER'],
                                                  app.config['UPLOAD_FOLDER'], 
                                                  app.config['COVER_SUBFOLDER'],
                                                  old_isbn)
                    cover_path = os.path.join(app.config['APP_FOLDER'],
                                              app.config['UPLOAD_FOLDER'], 
                                              app.config['COVER_SUBFOLDER'],
                                              book.isbn)
                    shutil.move(old_cover_path, cover_path)
                    img = os.path.join('/',
                                       app.config['UPLOAD_FOLDER'], 
                                       app.config['COVER_SUBFOLDER'],
                                       book.isbn,
                                       filename)
                    book.cover = img
                else:
                    book.cover = form.hidden_cover.data
                    img = book.cover
            # If there is not image (file) then we have to delete the older 
            # possible file and set with None the variables.
            else:
                path = os.path.join(app.config['APP_FOLDER'],
                                       app.config['UPLOAD_FOLDER'], 
                                       app.config['COVER_SUBFOLDER'],
                                       old_isbn)
                if os.path.exists(path):
                    shutil.rmtree(path)
                filename = None
                img = None
            db_session.merge(book)
            db_session.commit()
            ok = True
        except IntegrityError, e:
            flash(gettext('The book already exist.'), 'error')
            app.logger.exception('The book already exist: %s' % e)
            db_session.rollback()
        except SQLAlchemyError, e:
            flash(gettext('There have been a database error.'), 'error')
            app.logger.error('There have been a database error: %s' % e)
            db_session.rollback()
        except IOError, e:
            flash(gettext('There was an error uploading the cover.'), 'warning')
            app.logger.exception('There was an error uploading the cover: %s' % e)
            ok = False
            db_session.rollback()
            return render_template('newbook.html', form=form, img=img, ok=ok,
                                   update=True, readed=readed)
    else:
        form.cover.data = book.cover
        form.title.data = book.title
        form.isbn.data = book.isbn
        form.description.data = book.description
        form.format.data = book.br_format.name if book.br_format else ''
        form.subject.data = ', '.join([ b.name for b in book.subjects ]) \
            if book.subjects else ''
        form.pages.data = book.pages
        form.author.data = '; '.join([ a.surname + ', ' + a.name \
                                           for a in book.authors ])
        form.publisher.data = book.br_publisher.name \
            if book.br_publisher else ''

        img = book.cover
        ok = False

    return render_template('newbook.html', form=form, img=img, ok=ok,
                           update=True, readed=readed)


@app.route('/newbook', methods=['GET', 'POST'])
@app.route('/newbook/<string:wishlist>', methods=['GET', 'POST'])
@login_required
def newbook(wishlist=None):
    """The new book view.

    Get the form POST and create the new book
    """
    form = BookForm(request.form)
    img = None
    ok = False
    if request.method == 'POST' and form.validate_on_submit():
        # The cover (is a file)
        try:
            book = Book()
            result = bookform2book(form, book)
        
            file = request.files['cover']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                cover_path = os.path.join(app.config['APP_FOLDER'],
                                          app.config['UPLOAD_FOLDER'], 
                                          app.config['COVER_SUBFOLDER'],
                                          book.isbn)
                cover_filename_path = os.path.join(cover_path, filename)
                make_sure_path_exists(cover_path)
                file.save(cover_filename_path)
                book.cover = os.path.join('/',
                                          app.config['UPLOAD_FOLDER'], 
                                          app.config['COVER_SUBFOLDER'],
                                          book.isbn,
                                          filename)
                img = book.cover
            elif form.hidden_cover.data:
                book.cover = form.hidden_cover.data
                img = book.cover
            else:
                book.cover = None
        except Exception, e:
            flash(gettext('There was an error uploading the cover.'), 'warning')
            app.logger.exception('There was an error uploading the cover: %s' % e)
            book.cover = None
            ok = False
            return render_template('newbook.html', form=form, img=img, ok=ok, update=False)

        # Inserting... if all was good...
        if not result:
            return render_template('newbook.html', form=form, img=img, ok=ok, update=False)

        # Whose is this book?
        if 'user_id' in session:
            userbook = UserBook()
            userbook.core_user = CoreUser.query.get(session['user_id']).id
            book.users.append(userbook)
        try:
            db_session.begin()
            db_session.add(book)
            db_session.commit()

            # Insert the book into wishlist?
            if wishlist is not None:
                wishlist = BookWishlist()
                wishlist.id = userbook.id
                db_session.begin()
                db_session.add(wishlist)
                db_session.commit()

            g.idbook = book.id
            ok = True # With that it will show the modal window.
        except IntegrityError, e:
            flash(gettext('The book already exist.'), 'error')
            app.logger.exception('The book already exist: %s' % e)
            db_session.rollback()
        except SQLAlchemyError, e:
            flash(gettext('There have been a database error.'), 'error')
            app.logger.error('There have been a database error: %s' % e)
            db_session.rollback()
        except Exception, e:
            flash(gettext('There have been a weird error.'), 'error')
            app.logger.error('There have been a weird error: %s' % e)
            db_session.rollback()

    # We need img data to reload the image into form when an error is raised and
    # it shows the form again. The ok parameter is to say to the view if this book
    # was inserted correctly and it will show a modal with information.
    return render_template('newbook.html', form=form, img=img, ok=ok, update=False)


@app.route('/changebookreaded/<int:book_id>', methods=['GET', 'POST'])
@login_required
def changebookreaded(book_id):
    """Change book readed view.

    Get the id of the book to change and show the form to update it.
    """
    form = BookReadedForm(request.form)
    ok = False
    readed = True

    book = Book.query.get(book_id)
    if book:
        img = book.cover
    else:
        img = None

    br = BookReaded \
        .query \
        .join(UserBook, UserBook.id == BookReaded.id) \
        .filter(UserBook.book == book_id) \
        .filter(UserBook.core_user == session['user_id']) \
        .first()

    if request.method == 'POST' and form.validate_on_submit():
        try:
            result = bookreadedform2bookreaded(form, book_id, br)
            if not result:
                return render_template('newbookreaded.html', form=form, img=img,
                                       ok=ok, update=True)
            db_session.begin()
            db_session.add(br)
            db_session.commit()
            g.idbook = book_id
            ok = True # With that it will show the modal window.
        except IntegrityError, e:
            flash(gettext('The book already exist.'), 'error')
            app.logger.exception('The book already exist: %s' % e)
            db_session.rollback()
        except SQLAlchemyError, e:
            flash(gettext('There have been a database error.'), 'error')
            app.logger.error('There have been a database error: %s' % e)
            db_session.rollback()
        except IOError, e:
            flash(gettext('There was an error uploading the cover.'), 'warning')
            app.logger.exception('There was an error uploading the cover: %s' % e)
            ok = False
            db_session.rollback()
            return render_template('newbookreaded.html', form=form, img=img,
                                   ok=ok, update=True)
    else:
        form.start.data = br.start
        form.finish.data = br.finish
        form.opinion.data = br.opinion
        form.valoration.data = br.valoration
        for purchased in br.purchased:
            form.price.data = purchased.price
            form.purchased.data = purchased.purchased
            if purchased.br_purchasedbookshop:
                form.bookshop.data = purchased.br_purchasedbookshop.name

        img = book.cover
        ok = False

    return render_template('newbookreaded.html', form=form, img=img, ok=ok,
                           update=True, readed=readed)


@app.route('/newbookreaded/<int:book_id>', methods=['GET', 'POST'])
@login_required
def newbookreaded(book_id):
    """The new readed book view.

    Get the form POST and create a new book readed.
    """
    form = BookReadedForm(request.form)
    ok = False

    book = Book.query.get(book_id)
    if book:
        img = book.cover
    else:
        img = None

    if request.method == 'POST' and form.validate_on_submit():
        try:
            br = BookReaded()
            result = bookreadedform2bookreaded(form, book_id, br)
            if not result:
                return render_template('newbookreaded.html', form=form, img=img,
                                       ok=ok, update=False)

            db_session.begin()
            db_session.add(br)
            db_session.commit()
            g.idbook = book_id
            bw_object = get_wishlist_from_book_id(book_id)
            if bw_object is not None:
                db_session.begin()
                db_session.delete(bw_object)
                db_session.commit()
            ok = True # With that it will show the modal window.
        except IntegrityError, e:
            flash(gettext('The book already exist.'), 'error')
            app.logger.exception('The book already exist: %s' % e)
            db_session.rollback()
        except SQLAlchemyError, e:
            flash(gettext('There have been a database error.'), 'error')
            app.logger.error('There have been a database error: %s' % e)
            db_session.rollback()
        except Exception, e:
            flash(gettext('There have been a weird error.'), 'error')
            app.logger.error('There have been a weird error: %s' % e)
            db_session.rollback()

    return render_template('newbookreaded.html', form=form, img=img, ok=ok, update=False)


@app.route('/userbooks', methods=['GET', 'POST'])
@app.route('/userbooks/<int:page>', methods=['GET', 'POST'])
@app.route('/userbooks/<int:books_per_page>/<int:page>', methods=['GET', 'POST'])
@login_required
def userbooks(books_per_page=24, page=0):
    """The user books view.

    It shows the user books in a list from start to end. The wishlist does not
    appear. There is pagination.
    """
    where = 'userbooks'
    title = gettext('My books')
    start = books_per_page * page
    end = books_per_page * (page + 1)

    wishlist_idlist = BookWishlist.query.values(BookWishlist.id)
    the_query = Book \
        .query \
        .join(UserBook, UserBook.book == Book.id) \
        .filter(UserBook.core_user == session['user_id']) \
        .filter(not_(UserBook.id.in_(wishlist_idlist))) \
        .order_by(Book.id)

    total_rows = the_query.count()
    remainder = total_rows % books_per_page
    number_of_pages = (total_rows // books_per_page) + 1 \
        if remainder else total_rows // books_per_page

    books = the_query.slice(start, end)

    return render_template('booklist.html', books=books,
                           total_books=total_rows, page=page,
                           number_of_pages=number_of_pages,
                           where=where, title=title)


@app.route('/wishlist', methods=['GET', 'POSt'])
@app.route('/wishlist/<int:page>', methods=['GET', 'POST'])
@app.route('/wishlist/<int:books_per_page>/<int:page>', methods=['GET', 'POST'])
@login_required
def wishlist(books_per_page=24, page=0):
    """The user wishlist view.

    It shows the wishlist of the user. There is pagination. Also, the user can
    create another one.
    """
    where = 'wishlist'
    title = gettext('My wishlist')
    start = books_per_page * page
    end = books_per_page * (page + 1)

    the_query = Book \
        .query \
        .join(UserBook, UserBook.book == Book.id) \
        .join(BookWishlist, BookWishlist.id == UserBook.id) \
        .filter(UserBook.core_user == session['user_id']) \
        .order_by(Book.id)

    total_rows = the_query.count()
    remainder = total_rows % books_per_page
    number_of_pages = (total_rows // books_per_page) + 1 \
        if remainder else total_rows // books_per_page

    books = the_query.slice(start, end)

    return render_template('booklist.html', books=books,
                           total_books=total_rows, page=page,
                           number_of_pages=number_of_pages,
                           where=where, title=title)
