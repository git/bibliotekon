# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

import bcrypt
from functools import wraps
from flask import session, redirect, url_for, flash
from flask.ext.babel import gettext


def login_required(func):
    """
    Is a decorator that is used to denied login actions required.

    If you used this decorator then the user could not execute the action if a
    session with index 'user_id' does not exist. So, if there is an error test
    then the decorator gives you to login action with an error through flash.

    For example:
    @app.route('/logout', methods=['GET', 'POST'])
    @login_required
    def logout():
        if session:
            session.clear()
        return redirect(url_for('index'))
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not 'user_id' in session:
            flash(gettext('You can not execute this action, please sign in'), 'error')
            return redirect(url_for('login'))
        return func(*args, **kwargs)
    return decorated_view


def bcrypt_gen_password_hash(raw_pass, extra_salt=None):
    """
    Generate a salt for this new password.

    Args:
    - raw_pass: user submitted password
    - extra_salt: (optional) If this password is with stored with a
      non-database extra salt
    """
    if extra_salt:
        raw_pass = u"%s:%s" % (extra_salt, raw_pass)

    return bcrypt.hashpw(raw_pass, bcrypt.gensalt())


def bcrypt_check_password(raw_pass, stored_hash, extra_salt=None):
    """
    Check to see if this password matches.

    Args:
    - raw_pass: user submitted password to check for authenticity.
    - stored_hash: The hash of the raw password (and possibly extra
      salt) to check against
    - extra_salt: (optional) If this password is with stored with a
      non-database extra salt (probably in the config file) for extra
      security, factor this into the check.

    Returns:
      True or False depending on success.
    """
    if extra_salt:
        raw_pass = u"%s:%s" % (extra_salt, raw_pass)

    hashed_pass = bcrypt.hashpw(raw_pass, stored_hash)

    # It has been gotten from mediagoblin project. 
    #
    # Reduce risk of timing attacks by hashing again with a random
    # number (thx to zooko on this advice, which I hopefully
    # incorporated right.)
    #
    # See also:
    rand_salt = bcrypt.gensalt(5)
    randplus_stored_hash = bcrypt.hashpw(stored_hash, rand_salt)
    randplus_hashed_pass = bcrypt.hashpw(hashed_pass, rand_salt)

    return randplus_stored_hash == randplus_hashed_pass
