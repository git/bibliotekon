# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import or_

from bibliotekon.db.database import db_session, init_db
from bibliotekon.auth.models import CoreUser
from bibliotekon.auth import lib as auth_lib


def register_user(request, form):
    """Handle user registration """
    init_db()

    user = CoreUser()
    user.username = None if not form.username.data else form.username.data
    user.email = None if not form.email.data else form.email.data
    user.passwd = auth_lib.bcrypt_gen_password_hash(form.passwd.data)
    user.name = None if not form.name.data else form.name.data
    user.lname = None if not form.lname.data else form.lname.data

    db_session.begin()
    db_session.add(user)
    db_session.commit()

    return user


def update_user(user, form):
    """Handle user update information """
    init_db()

    user.username = None if not form.username.data else form.username.data
    user.email = None if not form.email.data else form.email.data
    if form.passwd.data:
        user.passwd = auth_lib.bcrypt_gen_password_hash(form.passwd.data)
    user.name = None if not form.name.data else form.name.data
    user.lname = None if not form.lname.data else form.lname.data

    db_session.begin()
    db_session.merge(user)
    db_session.commit()


def check_login(username, passwd, username_might_be_email=False):
    """Handle login action """

    search = (CoreUser.username == username)
    if username_might_be_email:
        search = or_(search, CoreUser.email == username)
    user = CoreUser.query.filter(search).first()
    if user:
        if auth_lib.bcrypt_check_password(passwd, user.passwd):
            return user
    return None
