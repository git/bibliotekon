# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from flask import render_template, request, Blueprint, flash, redirect, url_for
from flask import session
from flask.ext.babel import gettext

from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from bibliotekon import app
from bibliotekon.auth.forms import LoginForm, RegistrationForm
from bibliotekon.auth.models import CoreUser
from bibliotekon.auth.tools import register_user, check_login
from bibliotekon.auth.lib import login_required

mod = Blueprint('auth', __name__, template_folder = 'templates')


@mod.route('/login', methods=['GET', 'POST'])
def login():
    """The login view.
    
    Get and check login POST.
    """
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        user = check_login(form.username.data, form.passwd.data, True)
        if user:
            # set up login in session
            session['user_id'] = user.id
            if user.username and user.email:
                session['username'] = ", ".join(["%s" % (v) for v in {user.username, user.email}])
            elif user.username:
                session['username'] = user.username
            else:
                session['username'] = user.email
            return redirect(url_for('index'))
        flash(gettext('Username or password error'), 'error')
    return render_template('login.html', form=form)


@mod.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    """The log out view.

    Close the session.
    """
    if session:
        session.clear()
    return redirect(url_for('index'))


@mod.route('/register', methods=['GET', 'POST'])
def register():
    """The registration view.

    Get, check and execute the register POST.
    """
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        try:
            user = register_user(request, form)
            if user:
                flash(gettext('Thanks for registering, you can sign in'), 'success')
                return redirect(url_for('login'))
        except IntegrityError, e:
            flash(gettext('The username or e-mail is already registered.'), 'error')
            app.logger.exception('The username or e-mail is already registered: %s' % e)
    return render_template('register.html', form=form)
