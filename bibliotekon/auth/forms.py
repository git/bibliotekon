# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

from flask.ext.wtf import Form, TextField, PasswordField, Required, Email
from flask.ext.wtf import ValidationError, Optional, EqualTo
from flask.ext.babel import gettext


class LoginForm(Form):
    username = TextField('Username or e-mail', 
                         [Required(gettext('The username or e-mail is required.'))])
    passwd = PasswordField('Password', [Required(gettext('The password is required.'))])


def UsernameOrEmail(form, field):
    """
    Check if either username or email has content
    """
    if not form.username.data and not form.email.data:
        raise ValidationError(gettext('You has to put something into username or email.'))


class RegistrationForm(Form):
    username = TextField('Username', [UsernameOrEmail])
    email = TextField('E-mail', [Email(), UsernameOrEmail, Optional()])
    passwd = PasswordField('Password', [Required(gettext('The password is required.')),
                                        EqualTo('passwdrepeat', message='Passwords must match')])
    passwdrepeat = PasswordField('Password repeat')
    name = TextField('Name')
    lname = TextField('Last name')
