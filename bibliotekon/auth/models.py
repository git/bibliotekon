# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, Integer, String
from bibliotekon.db.database import Base

class CoreUser(Base):
    __tablename__ = 'core_user'
    __table_args__ = {'mysql_engine':'InnoDB'}
    id = Column(Integer, primary_key=True)
    username = Column(String(20), unique=True)
    email = Column(String(100), unique=True)
    passwd = Column(String(500), nullable=False)
    name = Column(String(50))
    lname = Column(String(100))

    def __init__(self, username=None, email=None, passwd=None,
                 name=None, lname=None):
        self.username = username
        self.email = email
        self.passwd = passwd
        self.name = name
        self.lname = lname
