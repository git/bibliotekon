# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, flash
from flask import render_template, session
from flask.ext.babel import Babel
from datetime import timedelta
from config import LANGUAGES

app = Flask(__name__)
app.config.from_object('config')

from bibliotekon.auth.views import mod as authMod
from bibliotekon.user.views import mod as userMod
from bibliotekon.views import *

app.register_blueprint(authMod, url_prefix='/auth')
app.register_blueprint(userMod, url_prefix='/user')


# The i18n and l10n with Flask-Babel
babel = Babel(app)


# The logging
file_handler = RotatingFileHandler(filename="app.log", maxBytes=100000,
                                   backupCount=5)
file_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
app.logger.addHandler(file_handler)


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())


@app.errorhandler(404)
@app.errorhandler(400)
def not_found(error):
    return render_template('error/40x.html', error=error)

@app.route('/')
def index():
    last_books = Book.query.order_by(Book.id.desc()).limit(4).all()
    return render_template('index.html', app_name='Bibliotekon',
                           last_books=last_books)
