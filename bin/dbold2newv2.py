# -*- coding: utf-8 -*-

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar. If not, see <http://www.gnu.org/licenses/>.


################################################################################
# This script only work with my other database and with my home folders.       #
################################################################################


from sqlalchemy import create_engine, Table, MetaData, select, and_
import shutil, os


catalog_old = MetaData('mysql://roman:roman@192.168.1.104/machafuko?charset=utf8')#&use_unicode=1')

catalog = MetaData('mysql://roman:roman@localhost/bibliotekon?charset=utf8')#&use_unicode=1')


def my_str(s):
    #return unicode(s, errors='replace')
    #return unicode(s)
    #return unicode(s)
    return s.encode('utf-8')


def load_basic_info():
    try:
        # Insert the publishers.
        table_old = Table('utam_publisher', catalog_old, autoload=True)
        table = Table('publisher', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'], name=my_str(row['name']))

        # Insert the formats.
        table_old = Table('utam_format', catalog_old, autoload=True)
        table = Table('format', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'], name=my_str(row['name']))

        # Insert the books.
        table_old = Table('utam_book', catalog_old, autoload=True)
        table = Table('book', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            if row['cover']:
                filename = row['cover'].replace('/machafuko/utamaduni/uploads/covers/', '')
                if os.path.exists('/home/roman/tmp/'+filename):
                    path = '/static/uploads/cover/' + row['isbn'] + '/'
                    cover = path + filename
                    dst_base_dir = '/home/roman/workspace/www/bibliotekon/bibliotekon'+path
                    if not os.path.exists(dst_base_dir):
                        os.mkdir(dst_base_dir)
                        shutil.copy('/home/roman/tmp/'+filename,
                                    dst_base_dir+filename)
                else:
                    cover = row['cover']
            else:
                cover = row['cover']
            table.insert().execute(id=row['id'],
                                   isbn=row['isbn'],
                                   title=my_str(row['title']),
                                   description=my_str(row['description']),
                                   cover=my_str(cover),
                                   publisher=row['publisher'],
                                   format=row['format'],
                                   pages=row['pages'])

    except Exception, e:
        print "Error: %s" % e


def load_author_information():
    try:
        # Insert the authors.
        table_old = Table('utam_author', catalog_old, autoload=True)
        table = Table('author', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'],
                                   name=my_str(row['name']),
                                   surname=my_str(row['surname']),
                                   photo=my_str(row['photo']))
            
        # Insert the book authors.
        table_old = Table('utam_book_author', catalog_old, autoload=True)
        table = Table('book_author', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'],
                                   book=row['book'],
                                   author=row['author'])

    except Exception, e:
        print "Error: %s" % e


def load_subject_information():
    try:
        # Insert the subjects.
        table_old = Table('utam_subject', catalog_old, autoload=True)
        table = Table('subject', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'], name=my_str(row['name']))
            
        # Insert the book authors.
        table_old = Table('utam_book_subject', catalog_old, autoload=True)
        table = Table('book_subject', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'],
                                   book=row['book'],
                                   subject=row['subject'])
            
    except Exception, e:
        print "Error: %s" % e


def load_bookshop_information():
    try:
        # Insert the bookshops.
        table_old = Table('utam_bookshop', catalog_old, autoload=True)
        table = Table('bookshop', catalog, autoload=True)
        rs = table_old.select().execute()
        for row in rs:
            table.insert().execute(id=row['id'],
                                   name=my_str(row['name']),
                                   logo=my_str(row['logo']))
            
    except Exception, e:
        print "Error: %s" % e


def load_readed_information():
    try:
        utam_book = Table('utam_book', catalog_old, autoload=True)
        utam_read = Table('utam_read', catalog_old, autoload=True)
        utam_purchased = Table('utam_purchased', catalog_old, autoload=True)
        user_book = Table('user_book', catalog, autoload=True)
        book_readed = Table('book_readed', catalog, autoload=True)
        purchased = Table('purchased', catalog, autoload=True)

        rs = select([utam_book.c.id,
                     utam_read.c.start,
                     utam_read.c.finish,
                     utam_read.c.opinion,
                     utam_read.c.valoration,
                     utam_purchased.c.price,
                     utam_purchased.c.bookshop]) \
                     .where(and_(utam_book.c.isbn == utam_read.c.isbn,
                                 utam_book.c.isbn == utam_purchased.c.isbn)) \
                     .execute()
        id = 1
        for row in rs:
            user_book.insert().execute(id=id,
                                       book=row[0],
                                       core_user=1,
                                       private=0)
            book_readed.insert().execute(id=id,
                                         start=row[1],
                                         finish=row[2],
                                         opinion=row[3],
                                         valoration=row[4])
            purchased.insert().execute(id=id,
                                       price=row[5],
                                       bookshop=row[6])
            id = id + 1

    except Exception, e:
        print "Error: %s" % e


load_basic_info()
load_author_information()
load_subject_information()
load_bookshop_information()
load_readed_information()
