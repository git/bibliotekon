#!/bin/bash

# This file is part of Bibliotekon.
#
# Copyright (c) 2013 Román Ginés Martínez Ferrández <rgmf@riseup.net>
#
# Bibliotekon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Bibliotekon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


################################################################################
# This script can be used to create and update the needed files to             #
# translate messages using Flask-Babel (gettext).                              #
#                                                                              #
# You need to pass to this script the language code, for example:              #
# sh pybabel_update.sh es                                                      #
# sh pybabel_update.sh en                                                      #
# sh pybabel_update.sh fr                                                      #
################################################################################

if [ $# -ne 1 ];
then
    echo "Error: use: $0 <language code - es, en, etc>"
    exit 1
fi

pybabel extract -F babel.cfg -o messages.pot bibliotekon
if [ $? -eq 0 ];
then
    # If the folder exist then we need to update.
    ls bibliotekon/translations/$1 2> /dev/null > /dev/null
    if [ $? -eq 0 ];
    then
        pybabel update -i messages.pot -d bibliotekon/translations
        if [ $? -eq 0 ];
        then
            emacs bibliotekon/translations/$1/LC_MESSAGES/messages.po
        fi

    # Otherwise, we have to update.
    else
        emacs messages.pot
        if [ $? -ne 0 ];
        then
            echo "Error: no existe el fichero messages.pot"
            exit 1
        fi
        pybabel init -i messages.pot -d app/translations -l $lan
    fi

    # Compile.
    if [ $? -eq 0 ];
    then
        pybabel compile -d bibliotekon/translations
    fi
fi